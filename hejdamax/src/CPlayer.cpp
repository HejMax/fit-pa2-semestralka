#include "CPlayer.h"
#include "CAsciiScale.h"

char nonBlockingIO()
{
    char c;

    initscr();
    noecho();
    timeout(1000);
    if ((c = getch()) == ERR)
    {
        c = 'N';
    }
    refresh();
    endwin();
    return c;
}


CPlayer::CPlayer(vector<shared_ptr<CImage>>::iterator & begin,
                vector<shared_ptr<CImage>>::iterator & end,
                CAsciiScale & scale) : m_begin(begin), m_end(end), m_scale(scale) 
{
    
}

void CPlayer::changeSequenceOrder()
{
    size_t index = 0;
    long long int firstPostion = -1;
    long long int secondPosition = -1;
    // This bool is used for discovering if the m_begin and m_end variables should be updated.
    bool ChangedOrderIsEmpty = m_imagesChangedOrder.empty();
    // starting option is one because we want to switch positions of at least two images in the sequence
    int option = 1;
    
    while(option == 1)
    {
        index = 0;
        for(auto iter = m_begin; iter != m_end + 1; iter++, index++)
        {
            cout << std::left << setw(13) << setfill(' ') << index << "\t";
            (*iter) -> printHeader();
            if(ChangedOrderIsEmpty)
            {
                m_imagesChangedOrder.push_back((*iter));
            }
            
        }
        if(ChangedOrderIsEmpty)
        {
            m_begin = m_imagesChangedOrder.begin();
            m_end = m_imagesChangedOrder.end() - 1;
            ChangedOrderIsEmpty = !ChangedOrderIsEmpty;
        }
        cout << "Select two indexes whose positions will be swapped" << endl;
        while(firstPostion < 0 || firstPostion > (long long int)m_imagesChangedOrder.size() ||
            secondPosition < 0 || secondPosition > (long long int)m_imagesChangedOrder.size())
        {
            cin >> firstPostion >> secondPosition;
            if(cin.fail())
            {
                firstPostion = -10;
                cin.clear();
                cin.ignore(10000,'\n');
            }
            if((firstPostion < 0 || firstPostion > (long long int)m_imagesChangedOrder.size() ||
            secondPosition < 0 || secondPosition > (long long int)m_imagesChangedOrder.size()))
            {
                cout << endl;
                cout << "\x1B[1mOne or both positions are invalid please try again\x1B[0m" << endl;
                cout << endl;
            }
        }
        if(firstPostion != secondPosition)
        {
            iter_swap(m_imagesChangedOrder.begin() + firstPostion, m_imagesChangedOrder.begin() + secondPosition);
        }
        firstPostion = secondPosition = -1;
        cout << "Would you like to change the order even more? Type 1 for yes. Any other input will be taken as no" << endl;
        cin >> option;
        if(cin.fail())
        {
            cin.clear();
            option = 0;
        }
    }
}

void CPlayer::blacklistFrames()
{
    int toContinue = true;
    while(toContinue == 1)
    {
        cout << "Select frame that you dont want to delete from animation." << endl;
        frameBlackList.insert(frameSelector());
        cout << "Would you like to blacklist more frames? Type 1 for yes. Any other input will be taken as no" << endl;
        cin >> toContinue;
        if(cin.fail())
        {
            cin.clear();
            toContinue = 0;
        }
    }
}

void CPlayer::removeFrameFromBlacklist()
{
    int toContinue = 1;
    size_t option = -1;
    auto end = frameBlackList.end();
    while(toContinue == 1)
    {
        cout << "Select frame that you want to readd to animation" << endl;
        cout << "Currently blacklisted frames are: ";
        for(auto iter = frameBlackList.begin(); iter != frameBlackList.end(); iter++)
        {
            if(iter != frameBlackList.begin())
            {
                cout << " ";
            }
            cout << (*iter);
        }
        cout << endl;
       
        cin >> option;

        if(end == frameBlackList.find(option))
        {
            cout << "This frame is not on the blacklist" << endl;
        }
        else
        {
            frameBlackList.erase(option);
            break;
        }
        
        cout << "Would you like to remove  another frame from the blacklist? Type 1 for yes. Any other input will be taken as no" << endl;
        cin >> toContinue;
        if(cin.fail())
        {
            cin.clear();
            toContinue = 0;
        }
    }
}

void CPlayer::displaySequence() const
{
    char pause = 'N';
    cout << "To pause the animation press space. To restart press space again" << endl;
    cout << "animation will start in 5 seconds." << endl;
    sleep(5);
    for(size_t i = 1; i <= numOfFrames; i++)
    {
        if(frameBlackList.end() == frameBlackList.find(i))
        {
            displayFrame(i);
        }
        pause = nonBlockingIO();
        if(pause == ' ')
        {
            pause = 'N';
            while(pause != ' ')
            {
               pause = nonBlockingIO();
            }
        }
    }
}

void CPlayer::displaySingle() const
{
    (*m_begin) -> printHeader();
    (*m_begin) ->showImage(m_scale);
}

void CPlayer::displaySingleFrame() const
{
    size_t option = frameSelector();
    if(frameBlackList.end() == frameBlackList.find(option))
    {
        displayFrame(option);
    }
    
}

void CPlayer::displayFrame(size_t option) const
{
    size_t from = option * FRAMESIZE - FRAMESIZE;
    size_t beginRow = 0;
    size_t endRow = 0;
    size_t height = 0;
    size_t imageHeight = 0;

    for(auto iter = m_begin; iter != m_end + 1; iter++ )
    {
        imageHeight = (*iter) -> getSize().first;
        height += imageHeight;
        if(height > from)
        {
            beginRow = imageHeight - (height - from);
            if(height >= (from + FRAMESIZE))
            {
                endRow = beginRow + FRAMESIZE;
                (*iter) -> displayRows(m_scale, beginRow, endRow);
                break;
            }
            else
            {
                (*iter) -> displayRows(m_scale, beginRow, (*iter) -> getSize().first);
                if(iter != m_end)
                {
                    (*(iter + 1)) -> displayRows(m_scale, 0, (FRAMESIZE - (imageHeight - beginRow)));
                }
                
                break;
            }
        }
    }
}
size_t CPlayer::frameSelector() const
{
    size_t option = -1;
    cout << "Please select which frame to display. Frames are counted from 1 to total number of frames." << endl;
    cout << "Total number of frames is: " <<  numOfFrames << endl;
    
    while(option  > numOfFrames || option == 0)
    {
        
        cin >> option;

        if(option > numOfFrames || option == 0)
        {
            cout << "Invalid selection" << endl;
        }
    }

    return option;
}

void CPlayer::calculateNumberOfFrames()
{
    size_t animationHeight = 0;
    for(auto iter = m_begin; iter != m_end + 1; iter++)
    {
        animationHeight += (*iter) -> getSize().first;
    }
    numOfFrames = animationHeight / FRAMESIZE;
    if(animationHeight % FRAMESIZE != 0)
    {
        numOfFrames += 1;
    }
}