#ifndef MENU_H
#define MENU_H
#include <iostream>
#include <iomanip>
#include <utility>
#include <vector>
#include <memory>
#include "CPngImg.h"
#include "CJpegImg.h"
#include "CPlayer.h"
#include "CTransform.h"
#include "CAsciiScale.h"
using namespace std;

/** 
* This class contains all the functions needed to display the menus for controlling the program.
* 
* Menu structure \n
*      <ul>
*      <li> Load Menu:
*          <ul>
*              <li> Load PNG
*              <li> Load JPEG
*          </ul>
*      <li> Transform Menu:
*      <ul>
*          <li> Flip image horizontally
*          <li> Flip image vertically
*          <li> Flip image both ways
*          <li> Rotate image
*          <li> Change brightness
*      </ul>
*      <li>Player Menu:
*          <ul>
*              <li> Upload new ASCII scale
*              <li> Show single image
*                      <li> Animation Menu:
*                      <ul>
*                           <li> Start animation
*                           <li> Delete frame
*                           <li> Undo frame delete
*                           <li> Display frame
*                      </ul>
*     </ul>
*/

class CMenu
{
public:
    /// Sets the default values of the current option and the parent option variables. This ensures that the program will display the main menu on start.
    CMenu();
    /// This function controls which menu gets printed or whether the program should end. \n
    ///  This is done through the sum of the current option and  the parent option variables.
    bool printMenu();
    /// This function displays the main menu.
    bool printMainMenu();
    /// This function displays the submenu that is used for uploading new images.
    void printLoadMenu();
    /// This function displays the submenu that transforms uploaded images.
    /// Transformations include flipping the image, rotating the image and changing its brightness.
    void printTransformMenu();
    /// This function displays the submenu containing options relating to image display. \n
    /// These options are: upload a new ASCII scale, display a single image, and the animation menu (options for displaying multiple images).
    void printPlayerMenu();
    /// This function displays the submenu controlling options related to displaying multiple images. \n
    /// The options are sorting the images, deleting frames and displaying the animation.
    void printAnimationMenu(CPlayer &player);
    /// This function displays the image selector. \n
    /// This is used in other menus where the user has to select an image.
    /// @returns vector<shared_ptr<CImage>>::iterator&
    int printImageSelect(vector<shared_ptr<CImage>>::iterator &img);
    /// This function displays the ASCII scale selector. \n
    /// This is used in other menus where the user has to select an ASCII scale.
    /// @returns CAsciiScale&
    int printAsciielect(CAsciiScale &scale) const;

private:
    int parent;
    int current;
    string m_defaultAscii = "src/config/default.txt";
    vector<shared_ptr<CImage>> m_images;
    vector<CAsciiScale> m_scales;
};
#endif /* MENU_H */