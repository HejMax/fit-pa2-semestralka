#ifndef MY_PNG_H
#define MY_PNG_H
#include <iostream>
#include <fstream>
#include <ostream>
#include <memory>
#include <png.h>
#include <setjmp.h>
#include "CImage.h"
using namespace std;

class CPngImg: public CImage
{
    public:
        ~CPngImg();
        ///This function loads a PNG image.
        /// @param[in]  string& This represents the file that should be uploaded and saved in memory.
        /// @returns  bool This represents whether the file was loaded correctly.
        bool loadImage(const string &fileName);
    private:
        int getPixel(uint64_t, uint64_t) const;
        friend int read_chunk_callback(png_struct * ptr, png_unknown_chunkp chunk);
        png_structp m_png_ptr;
        png_infop m_info_ptr;
        int m_bit_depth, m_color_type, m_interlace_type;
        png_bytep *m_row_pointers;
        const string m_type = "png";
};
#endif /* MY_PNG_H */