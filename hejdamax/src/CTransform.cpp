#include "CTransform.h"

shared_ptr<CTransformedImg> CTransform::flipHorizontal(vector<shared_ptr<CImage>>::iterator & image) //left to right
{
    shared_ptr<CTransformedImg> img = make_shared<CTransformedImg>();
    pair<uint64_t, uint64_t> size = (*image) -> getSize();
    img ->setTransformationParameters({-1, 0, 0, 1}, (*image), " flipped horizontal", size);

    cout << "Transformation done." << endl;
    return img;
}
shared_ptr<CTransformedImg> CTransform::flipVertical(vector<shared_ptr<CImage>>::iterator & image) //bottom up
{
    shared_ptr<CTransformedImg> img = make_shared<CTransformedImg>();
    pair<uint64_t, uint64_t> size = (*image) -> getSize();
    img ->setTransformationParameters({1, 0, 0, -1}, (*image), " flipped vertical", size);

    cout << "Transformation done." << endl;
    return img;
}
shared_ptr<CTransformedImg> CTransform::flipBoth(vector<shared_ptr<CImage>>::iterator & image)
{
    shared_ptr<CTransformedImg> img = make_shared<CTransformedImg>();
    pair<uint64_t, uint64_t> size = (*image) -> getSize();
    img ->setTransformationParameters({-1, 0, 0, -1}, (*image), " flipped both ways", size);

    cout << "Transformation done." << endl;
    return img;
}
shared_ptr<CTransformedImg> CTransform::rotate(vector<shared_ptr<CImage>>::iterator & image)
{
    int angle = 0;
    shared_ptr<CTransformedImg> img = make_shared<CTransformedImg>();
    pair<u_int64_t, u_int64_t> size;
    cout << "Please select number of degrees to rotate. " << endl;
    cout << "The options are 90, 180 or 270" << endl;
    while(angle != 90 && angle != 180 && angle != 270)
    {
        cin >> angle;
        if(cin.fail())
        {
            angle = -10;
            cin.clear();
            cin.ignore(10000,'\n');
        }
        if(angle != 90 && angle != 180 && angle != 270)
        {
            cout << "\x1B[1mWrong option please try again.\x1B[0m" << endl;
        }
    }
    switch(angle)
    {
        case 90:
            size = {(*image) -> getSize().second, (*image) -> getSize().first};
            img -> setTransformationParameters({0, -1, 1, 0}, (*image), " rotated 90", size);
            break;
        case 180:
            size = {(*image) -> getSize().first, (*image) -> getSize().second};
            img -> setTransformationParameters({-1, 0, 0, -1}, (*image), " rotated 180", size);
            break;
        case 270:
            size = {(*image) -> getSize().second, (*image) -> getSize().first};
            img -> setTransformationParameters({0, 1, -1, 0}, (*image), " rotated 270", size);
            break;
    }
    
    cout << "Transformation done." << endl;
    return img;
}
shared_ptr<CTransformedImg> CTransform::changeBrightness(vector<shared_ptr<CImage>>::iterator & image)
{
    int percentage = -200;
    pair<uint64_t, uint64_t> size = (*image) -> getSize();
    cout << "Please select by how much you want to change the brightness of the selected image" << endl;
    cout << "(For example -25 would darken the image by 25 percent. 25 would lighten the image by 25 percent). The range is -100 to 100" << endl;
    while(percentage > 100 || percentage < -100)
    {
        cin >> percentage;
        if(cin.fail())
        {
            percentage = -200;
            cin.clear();
            cin.ignore(10000,'\n');
        }
        if(percentage > 100 || percentage < -100)
        {
            cout << "\x1B[1mWrong option please try again.\x1B[0m" << endl;
        }
    }
    shared_ptr<CTransformedImg> img = make_shared<CTransformedImg>();
    if(percentage < 0)
    {
        img -> setTransformationParameters({1, 0, 0, 1}, (*image), " darkened by " + to_string((-1)*percentage) + "%", size, percentage);
    }
    else
    {
        img -> setTransformationParameters({1, 0, 0, 1}, (*image), " lightened by " + to_string(percentage) + "%", size, percentage);
    }
    
    cout << "Transformation done." << endl;
    return img;
}