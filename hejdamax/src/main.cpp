#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <png.h>
#include "jpeglib.h"
#include <vector>
#include <memory>
#include "CImage.h"
#include "CPngImg.h"
#include "CJpegImg.h"
#include "CMenu.h"
#include "CTransform.h"
#include "CPlayer.h"

/*! \mainpage Ascii-art
 *
 * \section intro_sec Introduction
 *
 * This program allows the user to upload images in two formats and then display them as ASCII art. \n
 * Users can also transform the uploaded images. 
 *
 * \section install_sec Running
 *
 * If you see a red warning before the main interface loads it means the program wasn't able to
 * load the default ASCII scale. This means that before displaying any images you have to upload the ASCII scale in the player menu manually. 
 * Without uploading an ASCII scale the program will not be able to
 * display anything.
 *
 */


using namespace std;
int main()
{
    CMenu menu;
    menu.printMenu();

    return 0;
}