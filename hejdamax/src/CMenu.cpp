#include "CMenu.h"

CMenu::CMenu() : parent(0), current(0) 
{
    CAsciiScale defaultAsciiScale;
    bool exceptionThrown = false;
     try
    {
        defaultAsciiScale.loadAscii(m_defaultAscii);
    }
    catch(const char * msg)
    {
        exceptionThrown = true;
        cout << "\x1b[31m\x1b[1m" << endl;
        cout << msg << endl;
        cout << "Default ASCII scale couldn't be uploaded. Please upload manually." << endl;
        cout << "\x1b[0m" << endl;
    }
    if(!exceptionThrown)
    {
        m_scales.push_back(defaultAsciiScale);
    }    
    
}

bool CMenu::printMenu()
{
    while(parent + current >= -1)
    {
        switch (parent + current)
        {
            case -1:
                return true;
                break;
            case 0:
                printMainMenu();
                break;
            case 1:
                printLoadMenu();
                break;
            case 2:
                printTransformMenu();
                break;
            case 3:
                printPlayerMenu();
                break;
            default:
                cout << "Wrong option please try again" << endl;
            break;
        }
    }   
   return true;
}

bool CMenu::printMainMenu()
{
    cout << "Welcome to the main interface, please select what you want to do:" << endl << endl;

    cout << "\x1B[1mOption 0:\x1B[0m Image loading" << endl;
    cout << "\x1B[1mOption 1:\x1B[0m Image Transformation" << endl;
    cout << "\x1B[1mOption 2:\x1B[0m Player" << endl;
    cout << "\x1B[1mOption -1:\x1B[0m Exit" << endl;
    parent = 0;
    int option = 0;
    while(1)
    {
        cout << "Select your option as option number: " ;
        cin >> option;
        if(cin.fail())
        {
            option = 15;
            cin.clear();
            cin.ignore(10000,'\n');
        }
        switch (option)
        {
            case 0:
                current = 1;
                if(printMenu())
                {
                    return true;
                }
                break;
            case 1:
                current = 2;
                if(printMenu())
                {
                    return true;
                }
                break;
            case 2:
                current = 3;
                if(printMenu())
                {
                    return true;
                }
                break;
            case -1:
                current -= 1;
                if(printMenu())
                {
                    return true;
                }
                break;
            default:
                cout << "Wrong option please try again" << endl;
                break;
        }
    }
}
void CMenu::printLoadMenu()
{
    parent = 0;
    int option = 0;
    string fileName;
    while(1)
    {
        cout << "\x1B[1mOption 0:\x1B[0m Load PNG image" << endl;
        cout << "\x1B[1mOption 1:\x1B[0m Load JPEG image" << endl;
        cout << "\x1B[1mOption -1:\x1B[0m Main menu" << endl;
        cout << "Select your option as option number: " ;
        cin >> option;
        if(cin.fail())
        {
            option = 15;
            cin.clear();
            cin.ignore(10000,'\n');
        }
        if(option == 0 || option == 1)
        {
            cout << "Set filename for file upload: ";
            cin >> fileName;
        }
    shared_ptr<CPngImg> png = make_shared<CPngImg>();
    shared_ptr<CJpegImg> jpeg = make_shared<CJpegImg>();     
        switch (option)
        {
            case 0:
                if(png->loadImage(fileName))
                {
                    m_images.push_back(png);
                }
                break;
            case 1:
                if(jpeg->loadImage(fileName))
                {
                    m_images.push_back(jpeg);
                }
                break;
            case -1:
                current -= 1;
                if(printMenu())
                {
                    return;
                }
                break;
            default:
                cout << endl;
                cout << "\x1B[1mWrong option please try again.\x1B[0m" << endl;
                cout << endl;
                break;
        }
    }
} 
void CMenu::printTransformMenu()
{
    parent = 0;
    int option = 0;
    CTransform transform;
    vector<shared_ptr<CImage>>::iterator transformImg;
    while(1)
    {
        cout << "\x1B[1mOption 0:\x1B[0m Flip image horizontal" << endl;
        cout << "\x1B[1mOption 1:\x1B[0m Flip image vertical" << endl;
        cout << "\x1B[1mOption 2:\x1B[0m Flip image both ways" << endl;
        cout << "\x1B[1mOption 3:\x1B[0m Rotate image" << endl;
        cout << "\x1B[1mOption 4:\x1B[0m Change Brightness image" << endl;
        cout << "\x1B[1mOption -1:\x1B[0m Main menu" << endl;
        cout << "Select your option as option number: " ; 
        cin >> option; 
        if(cin.fail())
        {
            option = 15;
            cin.clear();
            cin.ignore(10000,'\n');
        }      
        switch (option)
        {
            case 0:
                cout << "Please select image to flip horizontally: " << endl;
                if(printImageSelect(transformImg) == -1)
                {
                    break; 
                }
                m_images.push_back(transform.flipHorizontal(transformImg));
                break;
            case 1:
                cout << "Please select image to flip vertically: " << endl;
                if(printImageSelect(transformImg) == -1)
                {
                    break; 
                }
                m_images.push_back(transform.flipVertical(transformImg));
                break;
            case 2:
                cout << "Please select image to flip vertically and horizontally: " << endl;
                if(printImageSelect(transformImg) == -1)
                {
                    break; 
                }
                m_images.push_back(transform.flipBoth(transformImg));
                break;
            case 3:
                cout << "Please select image to rotate " << endl;
                if(printImageSelect(transformImg) == -1)
                {
                    break; 
                }
                m_images.push_back(transform.rotate(transformImg));
                break;
            case 4:
            cout << "Please select image to for which you want to change the brightness " << endl;
            if(printImageSelect(transformImg) == -1)
            {
                break; 
            }
            m_images.push_back(transform.changeBrightness(transformImg));
            break;
            case -1:
                current -= 2;
                if(printMenu())
                {
                    return;
                }
                break;
            default:
                cout << endl;
                cout << "\x1B[1mWrong option please try again.\x1B[0m" << endl;
                cout << endl;
                break;
        }
    }
}
void CMenu::printPlayerMenu()
{
    parent = 0;
    int option = 0;
    bool exceptionThrown = false;
    string filename;
    CAsciiScale newScale;
    CAsciiScale selectedScale;
    vector<shared_ptr<CImage>>::iterator begin;
    vector<shared_ptr<CImage>>::iterator end;
    while(1)
    {
        cout << "\x1B[1mOption 0:\x1B[0m Upload new ASCII scale" << endl;
        cout << "\x1B[1mOption 1:\x1B[0m Show single image" << endl;
        cout << "\x1B[1mOption 2:\x1B[0m Animation options" << endl;
        cout << "\x1B[1mOption -1:\x1B[0m Main menu" << endl;
        cout << "Select your option as option number: " ; 
        cin >> option; 
        if(cin.fail())
        {
            option = 15;
            cin.clear();
            cin.ignore(10000,'\n');
        }      
        switch (option)
        {
            case 0:
                cout << "Please write path to ASCII scale that you want to upload: " << endl;
                cin. ignore(1000, '\n' );
                getline(cin, filename);
                try
                {
                    newScale = CAsciiScale();
                    newScale.loadAscii(filename);
                }
                catch(const char * msg)
                {
                    cout << "\x1b[31m\x1b[1m" << endl;
                    cout << msg << endl;
                    cout << "Default ASCII scale couldn't be uploaded. Please upload manually." << endl;
                    cout << "\x1b[0m" << endl;
                    exceptionThrown = true;
                }
                if(!exceptionThrown)
                {
                    cout << "Upload completed." << endl;
                    m_scales.push_back(newScale);
                }
                break;
            case 1:
                cout << "Please select Ascii scale to use: " << endl;
                if(printAsciielect(selectedScale) == -1)
                {
                    break;
                }
                cout << "Please select image to display: " << endl;
                if(printImageSelect(begin) == -1)
                {
                    break; 
                }
                end = m_images.end() + 1;
                CPlayer(begin, end, selectedScale).displaySingle();
                break;
            case 2:
                {
                    //had to use curly brackets here because i need to display another menu which will use the player as input. This will control
                    //the options relating to displaying multiple images.
                    cout << "Please select Ascii scale to use: " << endl;
                    if(printAsciielect(selectedScale) == -1)
                    {
                        break;
                    }
                    cout << "Now selecting range of images to display. First image has to have its ID less than the last." << endl;
                    cout << "You will be able to change this order later." << endl;
                    cout << "Please select first image to display: " << endl;
                    if(printImageSelect(begin) == -1)
                    {
                        break;
                    }
                    cout << "Please select last image to display: " << endl;
                    if(printImageSelect(end) == -1)
                    {
                        break;
                    }
                    CPlayer player(begin, end, selectedScale);
                    printAnimationMenu(player);
                }
                break;
            case -1:
                current -= 3;
                if(printMenu())
                {
                    return;
                }
                break;
            default:
                cout << endl;
                cout << "\x1B[1mWrong option please try again.\x1B[0m" << endl;
                cout << endl;
                break;
        }
    }
}

void CMenu::printAnimationMenu(CPlayer & player)
{
    int option = 0;
    CAsciiScale selectedScale;
    player.calculateNumberOfFrames();

    while(1)
    {
        cout << "\x1B[1mOption 0:\x1B[0m Start Animation" << endl;
        cout << "\x1B[1mOption 1:\x1B[0m Change order of displayed images" << endl;
        cout << "\x1B[1mOption 2:\x1B[0m Delete frame (Before selecting this option \nI would reccomend displaying the frames so you know which frame youre deleting.)" << endl;
        cout << "\x1B[1mOption 3:\x1B[0m Re-add frame" << endl;
        cout << "\x1B[1mOption 4:\x1B[0m Display frame" << endl;
        cout << "\x1B[1mOption -1:\x1B[0m Player Menu" << endl;
        cout << "Select your option as option number: " ; 
        cin >> option; 
        if(cin.fail())
        {
            option = 15;
            cin.clear();
            cin.ignore(10000,'\n');
        }      
        switch (option)
        {
            case 0:
                player.displaySequence();
                break;
            case 1:
                player.changeSequenceOrder();
                break;
            case 2:
                player.blacklistFrames();
                break;
            case 3:
                player.removeFrameFromBlacklist();
                break;
            case 4:
                player.displaySingleFrame();
                break;
            case -1:
                return;
                break;
            default:
                cout << endl;
                cout << "\x1B[1mWrong option please try again.\x1B[0m" << endl;
                cout << endl;
                break;
        }
    }
}

 int CMenu::printImageSelect(vector<shared_ptr<CImage>>::iterator & img)
{
    long long int counter= 0;
    long long int option = -1;
    while(option < 0 || option >= counter)
    {
        cout << std::left << setfill(' ') << "Image number\t" << setw(122) << "File name\t" << setw(12) << "Image width\t" << setw(15) << "Image height" << endl;
        for(auto iter = m_images.begin(); iter != m_images.end(); iter++)
        {
            cout << std::left << setw(13) << setfill(' ') << counter << "\t";
            (*iter) -> printHeader();
            counter++;
        }
        cout << "Select image number to use: " << endl;
        cout << "Option -1 will return you to the menu" << endl;
        cin >> option;
        if(cin.fail())
        {
            option = -10;
            cin.clear();
            cin.ignore(10000,'\n');
        }
        else if(option < 0 || option >= counter || option == counter)
        {
            if(option == -1)
            {
                return -1;
            }
            cout << endl;
            cout << "\x1B[1mImage number not found. Please try again.\x1B[0m" << endl;
            cout << endl;
        }
        else
        {
            img = m_images.begin() + option;
        }
    }
    return option;
}

int CMenu::printAsciielect(CAsciiScale & scale) const
{
    long long int counter= 0;
    long long int option = -1;
    while(option < 0 || option >= counter)
    {
        cout << std::left << setw(20) << setfill(' ') << "Ascii scale index" << setw(48) << "Scale" << endl;
        for(auto iter = m_scales.begin(); iter != m_scales.end(); iter++)
        {
            cout <<  std::left << setw(20) << setfill(' ') << counter;
            (*iter).printScale();
            counter++;
        }
        cout << "Select Scale index to use: " << endl;
        cout << "Option -1 will return you to the menu" << endl;
        cin >> option;
        if(cin.fail())
        {
            option = -10;
            cin.clear();
            cin.ignore(10000,'\n');
        }
        else if(option < 0 || option >= counter || option == counter)
        {
            if(option == -1)
            {
                return -1;
            }
            cout << endl;
            cout << "\x1B[1mScale number not found. Please try again.\x1B[0m" << endl;
            cout << endl;
        }
        else
        {
            scale = *(m_scales.begin() + option);
        }
    }
    return option;
}