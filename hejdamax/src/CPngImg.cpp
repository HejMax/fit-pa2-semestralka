
#include "CPngImg.h"

bool CPngImg::loadImage(const string &fileName)
{
  FILE *fp;
  if (!(fp = fopen(fileName.c_str(), "rb")))
  {
    cout << "failed to open file" << endl;
    return false;
  }
  m_png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (!m_png_ptr)
  {
    return false;
  }
  m_info_ptr = png_create_info_struct(m_png_ptr);
  if(!m_info_ptr)
  {
    return false;
  }
  if (setjmp(png_jmpbuf(m_png_ptr)))
  {
    return false;
  }
  png_init_io(m_png_ptr, fp);
  //png_set_sig_bytes(m_png_ptr, 8);
  png_read_info(m_png_ptr, m_info_ptr);

  m_width = png_get_image_width(m_png_ptr, m_info_ptr);
  m_height = png_get_image_height(m_png_ptr, m_info_ptr);
  m_color_type = png_get_color_type(m_png_ptr, m_info_ptr);
  m_bit_depth = png_get_bit_depth(m_png_ptr, m_info_ptr);
  m_name = fileName;

  if (m_bit_depth == 16)
  {
    png_set_strip_16(m_png_ptr);
  }
  // turn palleted images to rgb for easy processing.
  if (m_color_type == PNG_COLOR_TYPE_PALETTE)
  {
    png_set_palette_to_rgb(m_png_ptr);
  }
  // PNG_COLOR_TYPE_GRAY_ALPHA is always 8 or 16bit depth.
  if (m_color_type == PNG_COLOR_TYPE_GRAY && m_bit_depth < 8)
  {
    png_set_expand_gray_1_2_4_to_8(m_png_ptr);
  }
  if (png_get_valid(m_png_ptr, m_info_ptr, PNG_INFO_tRNS))
  {
    png_set_tRNS_to_alpha(m_png_ptr);
  }
  // If color_type doesn't have an alpha channel then fill it with 0xff.
  if (m_color_type == PNG_COLOR_TYPE_RGB || m_color_type == PNG_COLOR_TYPE_GRAY || m_color_type == PNG_COLOR_TYPE_PALETTE)
  {
    png_set_filler(m_png_ptr, 0xFF, PNG_FILLER_AFTER);
  }
  // expand grayscale images to rgb because im doing a transformation to grayscale later and i want color uniformity.
  if (m_color_type == PNG_COLOR_TYPE_GRAY || m_color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
  {
    png_set_gray_to_rgb(m_png_ptr);
  }
  png_read_update_info(m_png_ptr, m_info_ptr);

  m_row_pointers = (png_bytep *)malloc(sizeof(png_bytep) * m_height);
  for (uint64_t y = 0; y < m_height; y++)
  {
    m_row_pointers[y] = (png_byte *)malloc(png_get_rowbytes(m_png_ptr, m_info_ptr));
  }

  png_read_image(m_png_ptr, m_row_pointers);

  fclose(fp);
  cout << "Upload completed." << endl;
  return true;
}

int CPngImg::getPixel(uint64_t y_position, uint64_t x_position) const
{
  png_bytep row = m_row_pointers[y_position];
  png_bytep px = &(row[x_position * 4]);
  return ((int)px[0] + (int)px[1] + (int)px[2])/3;
}

CPngImg::~CPngImg()
{
  png_destroy_read_struct(&m_png_ptr, &m_info_ptr, NULL);
  for (uint64_t y = 0; y < m_height; y++)
  {
    free(m_row_pointers[y]);
  }
  free(m_row_pointers);
}