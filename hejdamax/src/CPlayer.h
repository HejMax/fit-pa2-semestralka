#ifndef PLAYER_H
#define PLAYER_H
#include <iostream>
#include <utility>
#include <vector>
#include <set>
#include <memory>
#include "CImage.h"
#include <ncurses.h>
#include <unistd.h>
/**
 * This class controls the displaying of images using a selected ASCII scale. The two display methods are: \n
 * \n
 * Display single image \n
 * &nbsp;&nbsp;&nbsp;&nbsp;This displays a single image. \n
 * \n
 * Display animation \n
 * &nbsp;&nbsp;&nbsp;&nbsp;This displays a sequence of images. There are more options to further customize the output.
 * 
 * 
*/

class CPlayer
{
    public:
        CPlayer() = default;
        CPlayer(vector<shared_ptr<CImage>>::iterator & begin,
                vector<shared_ptr<CImage>>::iterator & end,
                CAsciiScale & scale);
        /// Changes the order of images in the animation. \n \n 
        /// If you have deleted some frames and then changed the order
        /// this will mess up the deletion of those frames. I recommend re-adding those frames and then deleting them again. 
        void changeSequenceOrder();
        /// Diplays multiple images in an "animation". Pausing the animation is supported.
        void displaySequence() const;
        /// Displays a single image.
        void displaySingle() const;
        /// Displays a single frame from the animation. 
        void displaySingleFrame() const;
        /// Calculates the number of frames in the animation.
        void calculateNumberOfFrames();
        /// This function displays a single frame specified by the input parameter. \n
        /// This is useful for figuring out which frame to delete.
        /// @param[in] size_t The ID of the frame to be displayed. ID is the position of the frame in the sequence of images. The frame ID begins with 1.
        void displayFrame(size_t option) const;
        /// Blacklists a frame from being displayed in an animation and in a single frame display.
        void blacklistFrames();
        /// This allows the user to remove a frame from the blacklist.
        void removeFrameFromBlacklist();
    private:
        size_t frameSelector() const;
        /// FRAMESIZE tells us how many rows is one frame (default value is 25 rows)
        const size_t FRAMESIZE = 25; 
        vector<shared_ptr<CImage>>::iterator & m_begin;
        vector<shared_ptr<CImage>>::iterator & m_end;
        CAsciiScale & m_scale;
        vector<shared_ptr<CImage>> m_imagesChangedOrder;
        set<size_t> frameBlackList;
        size_t numOfFrames = 0;

};
#endif /* PLAYER_H */