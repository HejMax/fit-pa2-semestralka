#ifndef IMAGE_H
#define IMAGE_H
#include <iostream>
#include <iomanip>
#include <memory>
#include <png.h>
#include "CAsciiScale.h"

using namespace std;

/**
* Class containing the methods used for image handling. \n
* The classes responsible for the different image types are derived from this class (jpeg, png, transformed).
*/

class CImage
{
    public: 
        ///This function loads a basic image.
        /// @param[in]  string& This represents the file which should be uploaded and saved in memory.
        /// @returns  bool This represents whether the file was loaded correctly.
        virtual bool loadImage(const string &fileName ) = 0;
        /// Writes the image to stdout.
        void showImage(CAsciiScale & scale) const 
        {
            for (uint64_t y = 0; y < m_height; y++)
            {
                for (uint64_t x = 0; x < m_width; x++)
                {
                    int pixel = getPixel(y, x);
                    scale.displayCharacter(pixel);
                }
                cout << endl;
            }
        };
        /// This function writes some of the rows of the image to stdout.
        /// This will be used for displaying animations.
        /// @param[in] CAsciiScale& The ASCII scale that is used to display the given rows.
        /// @param[in] size_t This tells us from which row to print (for example: 1 would mean start from the first row).
        /// @param[in] size_t This tells us which line is the last to be printed (for example 10 would mean end with the 10th row).
        void displayRows(CAsciiScale& scale, size_t from, size_t to) const
        {
            for (uint64_t y = from; y < to; y++)
            {
                for (uint64_t x = 0; x < m_width; x++)
                {
                    int pixel = getPixel(y, x);
                    scale.displayCharacter(pixel);
                }
                cout << endl;
            }
        }
        /// Getpixel returns a grayscale representation of the color of the pixel in the given position.\n
        /// @param [in]  uint64_t y position
        /// @param [in]  uint64_t x position
        /// @return int from 0 to 255 that is then mapped to a character on the ASCII scale.
        virtual int getPixel(uint64_t, uint64_t) const = 0 ;
        /// @return pair<uint64_t, uint64_t> where the first item is height and the second is width.
        pair<uint64_t, uint64_t> getSize() const {return {m_height, m_width};};
        /// Writes the image header to stdout. \n
        /// This is used in CMenu for image selection.
        void printHeader() const {cout << std::left << setfill(' ') << setw(120) << m_name << '\t' << setw(15) << m_width << '\t' << setw(15) << m_height << endl;};
        /// Returns the image name.
        /// @return string representing the image name.
        string getName() const {return m_name;};
    protected:
        uint64_t m_width;
        uint64_t m_height;
        string m_name;
};
#endif /* IMAGE_H */